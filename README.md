#### How would you design the system?

I would design the system by starting of with S3 as a single source which is highly available and durable where data in batches from different sources can be ingested. Data stored in S3 can be consumed with the help of Apache Airflow using Pandas. Apache Airflow will schedule and monitor workflows, help in monitoring the Jobs written in Panda which is deployed in Kubernetes for high availibility.

#### How would you set up monitoring to identify bottlenecks as the load grows?

For Monitoring our workflows, Airflow have native capability to monitor different Jobs, see the logs and watch different metrics for better visualization and debugging which helps in identifying bottelnecks. Jobs itself will be running in Kubernetes and we will integrate DataDog for monitoring our Kubernetes Cluster.

#### How can those bottlenecks be addressed in the future?

Firstly we will identify where we are having the bottelneck by seeing the metrics/logs and performance. We can then address the issue by either optimizing the technologies we are using or do research on how we can solve this by using any other Opensource projects or any Cloud services

Provide a high-level diagram, along with a few paragraphs describing the choices you've made and what factors you need to take into consideration.

![](./Xeneta-Data-Ingestion-Pipeline-Highlevel.png)

I have chosen S3 as a single source (Data Lake), we are using S3 as it is highly available and durable service from AWS, we can store as much data we want with less configuration and store it in any storage class depending on our need. Apache Airflow will consume data from S3 and further ETL processing starts. We are using Apache Airflow as Workflow management is easy, Task dependency management, Automation of queries, Monitoring, Alerting system and management is easier. After processing the data, we will be loading that data to Redshift or PostgreSQL depending on our use case. Redshift is highly available service and secure, it is easier to setup, we can scale Redshift as per our needs, schedule the nodes to scale-up/down at particular time for cost optimization and it can connect to most data sources via Python, JDBC or ODBC drivers.

#### The batch updates have started to become very large, but the requirements for their processing time are strict.

We can increase processing time by increasing the number of Pandas Jobs.

#### Code updates need to be pushed out frequently. This needs to be done without the risk of stopping a data update already being processed, nor a data response being lost.

We will setup Pipeline for CI/CD in Kubernetes, we can use ArgoCD for this purpose. To prevent already running process we will be using Blue/Green deployment.

For development and staging purposes, you need to start up a number of scaled-down versions of the system.

For Dev and Staging we can use small Kubernetes cluster. Redshift can be used with Single Node for preventing higher cost in Dev/Staging environments.

### Please address at least one of the situations. Please describe:

#### Which parts of the system are the bottlenecks or problems that might make it incompatible with the new requirements?
#### How would you restructure and scale the system to address those?

Bottelneck can occur and these are the two areas where we can expect as load increases:
* AWS S3: S3 bucket can have 3,500 PUT/COPY/POST/DELETE or 5,500 GET/HEAD requests per second per partitioned prefix.
* Pandas: can have issues if the load increases, horizontal scaling isn't optimized.

How would you restructure and scale the system to address those?
* AWS S3: We can add more prefixes to our bucket to increase the PUT/COPY/POST/DELETE request per second.
* Pandas: We can move from Pandas to Apache Spark as we can take advantage of horiztal scaling per our load.